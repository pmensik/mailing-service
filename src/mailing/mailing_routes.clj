(ns mailing.mailing-routes
  (:require [compojure.core :refer :all]
            [taoensso.timbre :as timbre]
            [validateur.validation :refer [valid?]]
            [cheshire.core :as json]
            [ring.util.response :refer [response get-header]]
            [mailing.mail-sender :as sender]
            [mailing.validation.email-validator :as validator]))

; Output of echo "mailing_password" | md5sum | cut -c1-12
(def auth-token "151658fd4a6c")
(def auth-header "Auth-Token")

(defn- generate-response
  "Generates either correct or bad request response"
  [errors]
  (if (empty? errors)
    (response json/generate-string "All emails have been sent.")
    (let [response-map {:status 400
                        :headers {"Content-Type" "application/json"}}]
      (assoc response-map :body (json/generate-string 
                                  {:message (str (count errors) " emails have not been sent.")
                                   :errors errors})))))

(defn- get-unauthorized-response
  [body]
  {:status 401
   :body (json/generate-string {:error body})
   :headers {"Content-Type" "application/json"}})

(defn- send-email
  "Sends email or returns error map"
  [email]
  (let [errors (validator/validate-email-json email)]
    (if (empty? errors)
      (sender/send-email email)
      (do 
        (timbre/error "Wrong format of email has been submitted")
        {:error errors :email email}))))

(defn- add-error
  "Adds error to transient vector"
  [error-coll result]
  (when-not (nil? result)
    (conj! error-coll result)))

(defn send-emails
  "Sends all emails"
  [emails]
  (let [response-errors (transient [])]
    (if (seq? emails)
      (do 
        (timbre/info "Sending" (count emails) "emails.")
        (doseq [email emails]
          (add-error response-errors (send-email email))))
      (add-error response-errors (send-email emails)))
    (generate-response (persistent! response-errors))))

(defn- authorize 
  "Checks users token before sending emails"
  [request]
  (let [header (get-header request auth-header)]
    (if (nil? header)
      (get-unauthorized-response "No authorization header provided")
      (if (= header auth-token)
        (send-emails (:body-params request))
        (get-unauthorized-response "Bad authorization token provided")))))
      

(defroutes mailing-routes
  (POST "/send-email" request (authorize request)))
