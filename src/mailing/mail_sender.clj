(ns mailing.mail-sender
  (:require [mailing.config.common-config :refer [smtp-config]]
            [postal.core :as postal]
            [taoensso.timbre :as timbre]))

(defn send-email
  "Sends email with content to the user"
  [email]
  (let [address (:address email)
        subject (:subject email)]
    (timbre/info "Sending email to" address "with subject" subject)
    (postal/send-message 
      smtp-config
      {:from (:from smtp-config)
       :to address
       :subject subject
       :body [{:type "text/html"
               :content (:body email)}]})))
