(ns mailing.validation.email-validator
  (:require [validateur.validation :refer :all]))

(def subject-validator
  (validation-set
    (presence-of :subject
                 :message "Zadejte předmět")))

(def body-validator
  (validation-set
    (presence-of :body
                 :message "Zadejte předmět")))

(def address-validator
  (validation-set
    (presence-of :address
                  :message "Zadejte adresu")))

(defn validate-email-json
  "Validates input from the user when submitting import form"
  [import-form]
  ((compose-sets address-validator subject-validator body-validator) import-form))
