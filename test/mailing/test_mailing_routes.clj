(ns mailing.test-mailing-routes
  (:require [mailing.handler :refer [app]]
            [mailing.mailing-routes :refer [auth-token]]
            [midje.sweet :refer :all]
            [cheshire.core :as json]
            [peridot.core :refer :all]))

(defn- create-request
  ([] (create-request nil))
  ([headers]
   (create-request headers nil))
  ([headers body]
   (-> (session app)
       (request "/send-email"
                :request-method :post
                :content-type "application/json"
                :headers
                  (when-not (nil? headers)
                    headers)
                :body
                  (when-not (nil? body)
                    body))
       (:response))))

(facts "Test send-mail endpoint with bad tokens"
  (fact "Missing auth header should result in 401 and exception"
    (let [response (create-request)]
      response => (contains [[:status 401]])
      (-> :body response json/parse-string) => {"error" "No authorization header provided"}))

  (fact "Invalid auth header should result in 401 and exception"
    (let [response (create-request {"Auth-Token" "bad-token"})]
      response => (contains [[:status 401]])
      (-> :body response json/parse-string) => {"error" "Bad authorization token provided"})))

(facts "Test response when emails have invalid format"
  (fact "Sending 1 email with empty body return errors vector with size 1"
    (let [response (create-request {"Auth-Token" auth-token})]
      response => (contains [[:status 400]])
      (-> :body response (json/parse-string true) :errors count) => 1))

   (fact "Sending 3 emails with empty body return errors vector with size 3"
    (let [response (create-request {"Auth-Token" auth-token} (json/generate-string [{} {} {}]))]
      response => (contains [[:status 400]])
      (-> :body response (json/parse-string true) :errors count) => 3)))
