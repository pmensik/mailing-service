(defproject
  mailing-service
  "0.1.0-SNAPSHOT"
  :description
  "Email Sender with RESTfull API"
  :url
  "http://example.com/FIXME"
  :min-lein-version "2.0.0"
  :dependencies
   [[org.clojure/clojure "1.8.0"]
   [prone "1.1.2"]
   [noir-exception "0.2.5"]
   [com.taoensso/timbre "4.7.4"]
   [lib-noir "0.9.9"]
   [environ "1.1.0"]
   [ring "1.5.0"]
   [compojure "1.5.1"]
   [com.draines/postal "2.0.0"]
   [com.novemberain/validateur "2.5.0"]
   [ring/ring-json "0.4.0"]
   [cheshire "5.6.3"]]
  :jvm-opts
  ["-server"]
  :plugins
  [[lein-ring "0.8.13"]
   [lein-environ "1.0.0"]
   [lein-ancient "0.5.5"]]
  :main ^:skip-aot mailing.handler
  :profiles
    {:uberjar {:omit-source true
               :aot :all}
     :dev {:dependencies [[ring/ring-mock "0.3.0"]
                         [midje "1.6.3"]
                         [peridot "0.4.4"]]
           :ring {:open-browser? false}
           :plugins [[lein-midje "3.2.1"] ]}})
